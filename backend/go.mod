module bitbucket.org/leogsouza/le_so/backend

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/gorm v1.9.10
	github.com/joho/godotenv v1.3.0
	github.com/leogsouza/udemy-clone v0.0.0-20181202005902-b3f6d9e07b11
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c
)
