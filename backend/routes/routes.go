package routes

import (
	"bitbucket.org/leogsouza/le_so/backend/handlers"
	"github.com/gorilla/mux"
)

// Handlers returns a Router with all handled routes
func Handlers() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/register", handlers.CreateUser).Methods("POST")
	router.HandleFunc("/register/google", handlers.CreateUserGoogle).Methods("POST")
	router.HandleFunc("/login", handlers.Login).Methods("POST")

	router.HandleFunc("/profile", handlers.JwtVerify(handlers.GetUser)).Methods("GET")
	router.HandleFunc("/profile", handlers.JwtVerify(handlers.UpdateUser)).Methods("PUT")

	return router

}
