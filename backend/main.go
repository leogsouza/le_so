package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/leogsouza/le_so/backend/common"
	"bitbucket.org/leogsouza/le_so/backend/models"

	"bitbucket.org/leogsouza/le_so/backend/routes"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
)

func main() {

	db := common.InitDB()
	db.AutoMigrate(
		&models.User{})

	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error loading .env file")
	}
	port := os.Getenv("PORT")

	http.Handle("/", routes.Handlers())

	// serve
	log.Printf("Server up on port '%s'", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
