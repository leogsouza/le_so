package models

import "github.com/dgrijalva/jwt-go"

// Token represents a token to be used by a logged user
type Token struct {
	UserID uint
	Email  string
	*jwt.StandardClaims
}
