package models

import (
	"errors"
	"time"

	"bitbucket.org/leogsouza/le_so/backend/common"
	"github.com/jinzhu/gorm"
	// to use mysql db
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"golang.org/x/crypto/bcrypt"
)

var db *gorm.DB

// User represents a user  into database
type User struct {
	ID        uint       `json:"id" gorm:"primary_key"`
	Fullname  string     `json:"fullname" gorm:"size:255"`
	Address   string     `json:"address"  gorm:"size:255"`
	Email     string     `json:"email" gorm:"not null; unique;size:255"`
	Password  string     `json:"password" gorm:"not null;size:255"`
	Telephone string     `json:"telephone"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-"`
}

// CreateUser saves a new user in the database
func CreateUser(model *User) error {
	db = common.InitDB()
	defer db.Close()
	err := db.Save(model).Error

	return err
}

// GetUserLogin returns a user in the database or an error
func GetUserLogin(email, password string) (*User, error) {
	user := &User{}
	db = common.InitDB()
	defer db.Close()
	if err := db.First(user, "email=?", email).Error; err != nil {
		return nil, errors.New("Email address not found")
	}

	errf := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if errf != nil && errf == bcrypt.ErrMismatchedHashAndPassword {
		return nil, errors.New("Invalid login credentials. Please try again")
	}
	return user, nil
}

// GetProfileUser returns the informations about user profile
func GetProfileUser(id uint) (*User, error) {
	user := &User{}

	db = common.InitDB()
	defer db.Close()

	if err := db.First(user, "id=?", id).Error; err != nil {
		return nil, errors.New("User not found")
	}
	return user, nil
}

// UpdateProfileUser updates an user profile
func UpdateProfileUser(id uint, model *User) error {
	user := &User{}

	db = common.InitDB()
	defer db.Close()

	if err := db.First(user, "id=?", id).Error; err != nil {
		return errors.New("User not found")
	}

	err := db.Model(user).Updates(model).Error

	return err
}
