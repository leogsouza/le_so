package common

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

// InitDB initialize a connection DB
func InitDB() *gorm.DB {

	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error on loading .env file")
	}

	dbuser := os.Getenv("DB_USER")
	dbpass := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")
	dbhost := os.Getenv("DB_HOST")

	dburi := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local", dbuser, dbpass, dbhost, dbname)

	db, err := gorm.Open("mysql", dburi)

	if err != nil {
		fmt.Println("error", err)
		panic(err)
	}
	return db
}
