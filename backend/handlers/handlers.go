package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"bitbucket.org/leogsouza/le_so/backend/models"
	jwt "github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

// LoginResponse represents a response from login
type LoginResponse struct {
	Token string        `json:"token"`
	User  *UserResponse `json:"user"`
}

// UserResponse represents a reponse from profile requests
type UserResponse struct {
	ID        uint   `json:"id"`
	Fullname  string `json:"fullname"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	Telephone string `json:"telephone"`
}

// CreateUser performs a create user request
func CreateUser(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		respondError(w, http.StatusInternalServerError, "Password Encryption failed")
		return
	}

	user.Password = string(pass)

	err = models.CreateUser(user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	tokenString, err := generateToken(user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
	}

	loginResponse := &LoginResponse{}
	loginResponse.Token = tokenString
	loginResponse.User = &UserResponse{
		ID:        user.ID,
		Fullname:  user.Fullname,
		Address:   user.Address,
		Email:     user.Email,
		Telephone: user.Telephone,
	}

	respondJSON(w, http.StatusOK, loginResponse)
}

// CreateUserGoogle performs a create user request from google callback
func CreateUserGoogle(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()

	err := models.CreateUser(user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	tokenString, err := generateToken(user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
	}

	loginResponse := &LoginResponse{}
	loginResponse.Token = tokenString
	loginResponse.User = &UserResponse{
		ID:        user.ID,
		Fullname:  user.Fullname,
		Address:   user.Address,
		Email:     user.Email,
		Telephone: user.Telephone,
	}

	respondJSON(w, http.StatusOK, loginResponse)
}

// GetUser performs a get user data
func GetUser(w http.ResponseWriter, r *http.Request) {
	userID := r.Context().Value("user").(uint)

	profile, err := models.GetProfileUser(userID)

	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	userResponse := &UserResponse{
		ID:        profile.ID,
		Fullname:  profile.Fullname,
		Address:   profile.Address,
		Email:     profile.Email,
		Telephone: profile.Telephone,
	}

	respondJSON(w, http.StatusOK, userResponse)
}

// UpdateUser performs a update user request
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("entering updateUser")
	userID := r.Context().Value("user").(uint)
	user := &models.User{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	user.ID = userID
	fmt.Println("updateUser user", user)
	err := models.UpdateProfileUser(userID, user)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	userResponse := &UserResponse{
		ID:        user.ID,
		Fullname:  user.Fullname,
		Address:   user.Address,
		Email:     user.Email,
		Telephone: user.Telephone,
	}

	respondJSON(w, http.StatusCreated, userResponse)

}

// Login performs a login request
func Login(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}

	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		respondError(w, http.StatusBadRequest, "Invalid request")
		return
	}
	defer r.Body.Close()

	loggedUser, err := models.GetUserLogin(user.Email, user.Password)

	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
	}

	tokenString, err := generateToken(loggedUser)
	if err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
	}

	loginResponse := &LoginResponse{}
	loginResponse.Token = tokenString
	loginResponse.User = &UserResponse{
		ID:        loggedUser.ID,
		Fullname:  loggedUser.Fullname,
		Address:   loggedUser.Address,
		Email:     loggedUser.Email,
		Telephone: loggedUser.Telephone,
	}

	respondJSON(w, http.StatusOK, loginResponse)
}

// respondJSON makes the response with payload as json format
func respondJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

// respondError makes the error response with payload as json format
func respondError(w http.ResponseWriter, code int, message string) {
	respondJSON(w, code, map[string]string{"error": message})
}

func generateToken(user *models.User) (string, error) {
	expiresAt := time.Now().Add(time.Minute * 100000).Unix()

	tk := &models.Token{
		UserID: user.ID,
		Email:  user.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	return token.SignedString([]byte(os.Getenv("API_KEY_SECRET")))
}
