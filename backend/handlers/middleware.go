package handlers

import (
	"context"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/leogsouza/le_so/backend/models"
	jwt "github.com/dgrijalva/jwt-go"
)

// JwtVerify checks if token is valid
func JwtVerify(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var header = r.Header.Get("X-Auth-Token") //Grab the token from the header

		header = strings.TrimSpace(header)

		if header == "" {
			//Token is missing, returns with error code 403 Unauthorized
			respondError(w, http.StatusForbidden, "Missing auth token")
			return
		}
		tk := &models.Token{}

		_, err := jwt.ParseWithClaims(header, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("API_KEY_SECRET")), nil
		})

		if err != nil {

			respondError(w, http.StatusForbidden, err.Error())
			return
		}

		ctx := context.WithValue(r.Context(), "user", tk.UserID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
