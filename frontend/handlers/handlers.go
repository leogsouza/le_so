package handlers

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/joho/godotenv"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

const oauthGoogleURLAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

var googleOauthConfig *oauth2.Config

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	// Scopes: OAuth 2.0 scopes provide a way to limit the amount of access that is granted to an access token.
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  "http://localhost:3000/auth/google/callback",
		ClientID:     os.Getenv("GOOGLE_OAUTH_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_OAUTH_CLIENT_SECRET"),
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
}

var t *template.Template

// LoginRequest is used to send credentials to log in
type LoginRequest struct {
	Email    string
	Password string
}

// LoginResponse represents the response from login request
type LoginResponse struct {
	Token string        `json:"token"`
	User  *UserResponse `json:"user"`
	AuthenticatedResponse
}

// UserRequest represents the user data used on profile request
type UserRequest struct {
	Fullname  string `json:"fullname"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	Telephone string `json:"telephone"`
}

// UserResponse represents the response from profile request
type UserResponse struct {
	ID uint `json:"id"`
	UserRequest
	AuthenticatedResponse
}

// AuthenticatedResponse indicates if user is authenticated
type AuthenticatedResponse struct {
	IsLogged bool
}

// GoogleUserInfo contains informations about user
type GoogleUserInfo struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Picture       string `json:"picture"`
}

// Token represents a valid token
type Token struct {
	UserID uint
	Email  string
	*jwt.StandardClaims
}

func getURLRequest(url string) string {
	return os.Getenv("API_URL") + url
}

// OauthGoogleLogin performs a request to sign in on google
func OauthGoogleLogin(w http.ResponseWriter, r *http.Request) {

	// Create oauthState cookie
	oauthState := generateStateOauthCookie(w)

	/*
		AuthCodeURL receive state that is a token to protect the user from CSRF attacks. You must always provide a non-empty string and
		validate that it matches the the state query parameter on your redirect callback.
	*/
	u := googleOauthConfig.AuthCodeURL(oauthState)
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
}

// OauthGoogleCallback ix called after a requested sign in on google
func OauthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	// Read oauthState from Cookie
	oauthState, _ := r.Cookie("oauthstate")

	if r.FormValue("state") != oauthState.Value {
		log.Println("invalid oauth google state")
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	data, err := getUserDataFromGoogle(r.FormValue("code"))
	if err != nil {
		log.Println(err.Error())
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	userInfo := &GoogleUserInfo{}
	err = json.Unmarshal([]byte(data), userInfo)

	log.Println("userinfo", userInfo)

	// GetOrCreate User in your db.
	// Redirect or response with a token.
	// More code .....
	loginRequest := &LoginRequest{
		Password: r.FormValue("password"),
	}

	requestBody, err := json.Marshal(loginRequest)
	if err != nil {
		log.Fatalln(err)
	}
	resp, err := http.Post(getURLRequest("/register/google"), "application/json", bytes.NewBuffer(requestBody))
	loginResponse := &LoginResponse{}

	err = json.NewDecoder(resp.Body).Decode(loginResponse)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Get the JWT string from the cookie
	tknStr := loginResponse.Token
	fmt.Println("tknStr", tknStr)
	// Initialize a new instance of `Claims`
	tk := &Token{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	_, err = jwt.ParseWithClaims(tknStr, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("API_KEY_SECRET")), nil
	})

	http.SetCookie(w, &http.Cookie{
		Name:    "app_token",
		Value:   tknStr,
		Expires: time.Unix(tk.StandardClaims.ExpiresAt, 0),
	})

	http.Redirect(w, r, "/profile", http.StatusFound)
}

func generateStateOauthCookie(w http.ResponseWriter) string {
	var expiration = time.Now().Add(20 * time.Minute)

	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: "oauthstate", Value: state, Expires: expiration}
	http.SetCookie(w, &cookie)

	return state
}

func getUserDataFromGoogle(code string) ([]byte, error) {
	// Use code to get token and get user info from Google.

	token, err := googleOauthConfig.Exchange(context.Background(), code)
	if err != nil {
		return nil, fmt.Errorf("code exchange wrong: %s", err.Error())
	}
	log.Println("tokenAccess", token.AccessToken)
	response, err := http.Get(oauthGoogleURLAPI + token.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed read response: %s", err.Error())
	}
	return contents, nil
}

// LoginPageHandler renders login page
func LoginPageHandler(w http.ResponseWriter, r *http.Request) {

	t, _ = template.ParseFiles("views/layout.html", "views/login.html")

	t.ExecuteTemplate(w, "layout", &AuthenticatedResponse{})
}

// LoginHandler process login with user credentials
func LoginHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	loginRequest := &LoginRequest{
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	requestBody, err := json.Marshal(loginRequest)
	if err != nil {
		log.Fatalln(err)
	}
	resp, err := http.Post(getURLRequest("/login"), "application/json", bytes.NewBuffer(requestBody))
	loginResponse := &LoginResponse{}

	err = json.NewDecoder(resp.Body).Decode(loginResponse)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Get the JWT string from the cookie
	tknStr := loginResponse.Token
	fmt.Println("tknStr", tknStr)
	// Initialize a new instance of `Claims`
	tk := &Token{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	_, err = jwt.ParseWithClaims(tknStr, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("API_KEY_SECRET")), nil
	})

	http.SetCookie(w, &http.Cookie{
		Name:    "app_token",
		Value:   tknStr,
		Expires: time.Unix(tk.StandardClaims.ExpiresAt, 0),
	})

	http.Redirect(w, r, "/profile", http.StatusFound)
}

// LogoutHandler logs out the logged user
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:   "app_token",
		Value:  "",
		MaxAge: -1,
	})

	http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
}

// IndexHandler renders index page
func IndexHandler(w http.ResponseWriter, r *http.Request) {

	c, err := r.Cookie("app_token")

	if err != nil {
		if err == http.ErrNoCookie {
			// If the cookie is not set, return an unauthorized status
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}
		// For any other type of error, return a bad request status
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get the JWT string from the cookie
	tknStr := c.Value

	err = ValidateToken(tknStr)

	if err != nil {
		http.Redirect(w, r, "/login", http.StatusUnauthorized)
		return
	}

	t, _ = template.ParseFiles("views/layout.html", "views/index.html")

	t.ExecuteTemplate(w, "layout", &AuthenticatedResponse{})
}

// RegisterPageHandler shows user registration page
func RegisterPageHandler(w http.ResponseWriter, r *http.Request) {
	t, _ = template.ParseFiles("views/layout.html", "views/register.html")

	t.ExecuteTemplate(w, "layout", &AuthenticatedResponse{})
}

// RegisterPageHandler register the user
func RegisterHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	loginRequest := &LoginRequest{
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	requestBody, err := json.Marshal(loginRequest)
	if err != nil {
		log.Fatalln(err)
	}
	resp, err := http.Post(getURLRequest("/register"), "application/json", bytes.NewBuffer(requestBody))
	loginResponse := &LoginResponse{}

	err = json.NewDecoder(resp.Body).Decode(loginResponse)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Get the JWT string from the cookie
	tknStr := loginResponse.Token
	fmt.Println("tknStr", tknStr)
	// Initialize a new instance of `Claims`
	tk := &Token{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	_, err = jwt.ParseWithClaims(tknStr, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("API_KEY_SECRET")), nil
	})

	http.SetCookie(w, &http.Cookie{
		Name:    "app_token",
		Value:   tknStr,
		Expires: time.Unix(tk.StandardClaims.ExpiresAt, 0),
	})

	http.Redirect(w, r, "/profile", http.StatusFound)
}

// ProfilePageHandler shows user profile page
func ProfilePageHandler(w http.ResponseWriter, r *http.Request) {

	c, err := r.Cookie("app_token")
	log.Println("cookie", c)
	fmt.Println("err", err)
	if err != nil {
		if err == http.ErrNoCookie {
			// If the cookie is not set, return an unauthorized status
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}
		// For any other type of error, return a bad request status
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get the JWT string from the cookie
	tknStr := c.Value

	client := &http.Client{}
	req, _ := http.NewRequest("GET", getURLRequest("/profile"), nil)
	req.Header.Set("X-Auth-Token", tknStr)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
	}
	defer resp.Body.Close()

	userResponse := UserResponse{}

	// Use json.Decode for reading streams of JSON data
	if err := json.NewDecoder(resp.Body).Decode(&userResponse); err != nil {
		log.Println(err)
	}

	userResponse.IsLogged = true
	t := template.Must(template.ParseFiles("views/layout.html", "views/profile.html"))

	t.ExecuteTemplate(w, "layout", userResponse)
}

// ProfileEditPageHandler show user edit profile page
func ProfileEditPageHandler(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie("app_token")

	if err != nil {
		if err == http.ErrNoCookie {
			// If the cookie is not set, return an unauthorized status
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}
		// For any other type of error, return a bad request status
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get the JWT string from the cookie
	tknStr := c.Value

	err = ValidateToken(tknStr)

	if err != nil {
		http.Redirect(w, r, "/login", http.StatusUnauthorized)
		return
	}

	client := &http.Client{}
	req, _ := http.NewRequest("GET", getURLRequest("/profile"), nil)
	req.Header.Set("X-Auth-Token", tknStr)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
	}
	defer resp.Body.Close()

	userResponse := UserResponse{}

	// Use json.Decode for reading streams of JSON data
	if err := json.NewDecoder(resp.Body).Decode(&userResponse); err != nil {
		log.Println(err)
	}

	userResponse.IsLogged = true
	t := template.Must(template.ParseFiles("views/layout.html", "views/edit_profile.html"))

	t.ExecuteTemplate(w, "layout", userResponse)
}

// ProfileEditPostHandler updates user profile
func ProfileEditPostHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	userRequest := &UserRequest{
		Fullname:  r.FormValue("fullname"),
		Address:   r.FormValue("address"),
		Email:     r.FormValue("email"),
		Telephone: r.FormValue("telephone"),
	}

	requestBody, err := json.Marshal(userRequest)
	if err != nil {
		log.Fatalln(err)
	}
	c, err := r.Cookie("app_token")

	if err != nil {
		if err == http.ErrNoCookie {
			// If the cookie is not set, return an unauthorized status
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}
		// For any other type of error, return a bad request status
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get the JWT string from the cookie
	tknStr := c.Value

	err = ValidateToken(tknStr)

	if err != nil {
		http.Redirect(w, r, "/login", http.StatusUnauthorized)
		return
	}

	client := &http.Client{}
	req, err := http.NewRequest(http.MethodPut, getURLRequest("/profile"), bytes.NewBuffer(requestBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Auth-Token", tknStr)
	resp, err := client.Do(req)
	userResponse := &UserResponse{}

	err = json.NewDecoder(resp.Body).Decode(userResponse)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	http.Redirect(w, r, "/profile", http.StatusFound)
}

// ValidateToken checks if token is valid
func ValidateToken(tknStr string) error {
	// Initialize a new instance of `Claims`
	tk := &Token{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwt.ParseWithClaims(tknStr, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("API_KEY_SECRET")), nil
	})
	if !tkn.Valid {
		return errors.New("Token is invalid")

	}
	if err != nil {
		if err == jwt.ErrSignatureInvalid {

			return errors.New("Token signature's invalid")
		}
		return errors.New("There an error on get the token")
	}

	return nil
}
