package routes

import (
	"net/http"

	"bitbucket.org/leogsouza/le_so/frontend/handlers"
	"github.com/gorilla/mux"
)

const (
	STATIC_DIR = "/public"
)

// Handlers returns a Router with all handled routes
func Handlers() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", handlers.IndexHandler).Methods("GET")
	// OauthGoogle
	router.HandleFunc("/auth/google/login", handlers.OauthGoogleLogin).Methods("GET")
	router.HandleFunc("/auth/google/callback", handlers.OauthGoogleCallback).Methods("GET")
	router.HandleFunc("/login", handlers.LoginPageHandler).Methods("GET")
	router.HandleFunc("/login", handlers.LoginHandler).Methods("POST")
	router.HandleFunc("/logout", handlers.LogoutHandler).Methods("GET")

	router.HandleFunc("/register", handlers.RegisterPageHandler).Methods("GET")
	router.HandleFunc("/register", handlers.RegisterHandler).Methods("POST")
	router.HandleFunc("/profile", handlers.ProfilePageHandler).Methods("GET")
	router.HandleFunc("/profile/edit", handlers.ProfileEditPageHandler).Methods("GET")
	router.HandleFunc("/profile/edit", handlers.ProfileEditPostHandler).Methods("POST")

	router.
		PathPrefix(STATIC_DIR).
		Handler(http.StripPrefix(STATIC_DIR, http.FileServer(http.Dir("."+STATIC_DIR))))

	return router

}
