module bitbucket.org/leogsouza/le_so/frontend

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/joho/godotenv v1.3.0
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
)
