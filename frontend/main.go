package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/leogsouza/le_so/frontend/routes"
	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	port := os.Getenv("PORT")

	http.Handle("/", routes.Handlers())
	// serve
	log.Printf("Website up on port '%s'", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
